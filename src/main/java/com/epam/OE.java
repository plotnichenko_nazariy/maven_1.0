package com.epam;

public class OE {
    private int num1, num2, ros, sumOdd, sumEven;
    private int[] masOdd, masEven;
    public int bigOdd, bigEven;

    OE(){
        num1 = 0;
        num2 = 0;
        sumOdd = 0;
        sumEven = 0;
        ros = 1;
    }

    OE(int num1, int num2){
        this.num1 = num1;
        this.num2 = num2;
        sumOdd = 0;
        sumEven = 0;
        ros = (int) (0.5 + ((num2 - num1)/2));
        masEven = new int [ros + 1];
        masOdd = new int [ros + 1];
    }

    public void SetNum1(int a){
        num1 = a;
    }

    public void SetNum2(int a){
        num2 = a;
    }

    public int GetNum1(){
        return num1;
    }

    public int GetNum2(){
        return num2;
    }

    public void FillInMas()
    {
        int od = 0, ev = 0;
        for(int i = num1; i <= num2; i++)
        {
            if(i%2 == 0)
            {
                masOdd[od] = i;
                od++;
                sumOdd = sumOdd + i;
            }
            else {
                masEven[ev] = i;
                ev++;
                sumEven = sumEven + i;
            }
        }
        bigOdd = masOdd[od - 1];
        bigEven = masEven[ev - 1];
    }

    public void Print(){
        System.out.println("Odd number from the beginning till the end");
        for(int i = 0; i < ros + 1; i++) {
            System.out.println(masOdd[i]);
        }
        System.out.println("Even numbers from the end till the beginning");
        for(int i = ros; i >= 0; i--) {
            System.out.println(masEven[i]);
        }
        System.out.println("Sum of Odd = " + sumOdd);
        System.out.println("Sum of Even = " + sumEven);
    }
}
