package com.epam;

public class Fibonacci {
    public void FiboSet(int num1, int num2, int amount){
        int perOdd, perEven, amOdd = 1, amEven = 1, num3, num;
        if(num2 > num1)
            System.out.print("Fibonacci set: " + num1 + num2);
        else {
            num = num2;
            num2 = num1;
            num1 = num;
            System.out.print("Fibonacci set: " + num1 + " " + num2);
        }
        for(int i = 0; i < amount - 2; i++){
            num3 = num1 + num2;
            if(num3 % 2 == 0)
                amOdd++;
            else
                amEven++;
            num1 = num2;
            num2 = num3;
            System.out.print(" " + num3);
        }
        perOdd = ((amOdd*100)/amount);
        perEven = ((amEven*100)/amount);
        System.out.println("\nPercentage of odd numbers = " + perOdd + "%, even = " + perEven + "%");
    }
}
